# AddToReminiscence

This extension adds a button to your toolbar which lets you add the current page you're browsing to your Reminiscence instance. Currently, all pages will be saved to the 'AddToReminiscence' directory.

IMPORTANT NOTE: This extension requires a seperate Reminiscence instance. Reminiscence is a self hosted bookmarking server, which is available here: https://github.com/kanishka-linux/reminiscence